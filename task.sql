drop table Clients;
drop table Orders;

create table Clients(
client_id number(10) primary key,
client_name varchar2(50)
);

create table Orders(
order_id number(10) primary key,
client_id number(10),
order_sum number(10),
order_date date
);

insert into Clients values (1, 'Client1');
insert into Clients values (2, 'Client2');
insert into Clients values (3, 'Client3');

insert into Orders values(1, 1, 30, to_date('2018-07-01', 'YYYY-MM-DD'));
insert into Orders values(2, 1, 40, to_date('2018-07-01', 'YYYY-MM-DD'));
insert into Orders values(3, 1, 40, to_date('2018-07-01', 'YYYY-MM-DD'));
insert into Orders values(4, 2, 60, to_date('2018-07-01', 'YYYY-MM-DD'));
insert into Orders values(5, 3, 60, to_date('2018-07-01', 'YYYY-MM-DD'));
insert into Orders values(6, 3, 60, to_date('2018-07-01', 'YYYY-MM-DD'));

-- list of clients, which have an order with order_sum > 50
select client_name from Clients where client_id in(select distinct client_id from Orders where order_sum>50);
-- clients, whose total sum of orders exceeds 100
select client_name from Clients where client_id in(select client_id from Orders group by client_id having sum(order_sum)>100);
