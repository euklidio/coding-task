package com.marcinziolo.assignment.tree;

import java.util.HashMap;
import java.util.Map;

class BtnComparator implements IBtnComparator {
    public boolean compare(Btn firstNode, Btn secondNode) {
        return compare(firstNode, secondNode, new HashMap<>(), new HashMap<>(), "");
    }

    private boolean compare(Btn firstNode, Btn secondNode, Map<Btn, String> firstTreeNodes, Map<Btn, String> secondTreeNodes, String path) {

        // branches are identical
        if (firstNode == null && secondNode == null)
            return true;

        // structure of trees is different
        if (firstNode == null ^ secondNode == null)
            return false;

        // difference in values
        if (firstNode.value != secondNode.value)
            return false;

        String firstPath = firstTreeNodes.get(firstNode);
        String secondPath = secondTreeNodes.get(secondNode);

        // one of tree has cycle from given node, another no, different structure
        if (firstPath == null ^ secondPath == null)
            return false;

        // cycles start from the same node for both trees, result is true if both go back to the same nodes
        if (firstPath != null)
            return firstPath.equals(secondPath);

        // add nodes to maps
        firstTreeNodes.put(firstNode, path);
        secondTreeNodes.put(secondNode, path);

        // process left and right subtree
        return compare(firstNode.left, secondNode.left, firstTreeNodes, secondTreeNodes, path + "L")
                && compare(firstNode.right, secondNode.right, firstTreeNodes, secondTreeNodes, path + "R");
    }
}
