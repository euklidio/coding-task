package com.marcinziolo.assignment.tree;

class Btn {
    int value;
    Btn left;
    Btn right;

    Btn(int value, Btn left, Btn right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    Btn getLeft() {
        return left;
    }

    void setLeft(Btn left) {
        this.left = left;
    }

    Btn getRight() {
        return right;
    }

    void setRight(Btn right) {
        this.right = right;
    }
}
