package com.marcinziolo.assignment.tree;

public interface IBtnComparator {
    boolean compare(Btn firstNode, Btn secondNode);
}
