package com.marcinziolo.assignment.filter;

class Element {
    int num;
    private String name;
    int age;

    Element(String name, int num, int age) {
        this.num = num;
        this.name = name;
        this.age = age;
    }
}
