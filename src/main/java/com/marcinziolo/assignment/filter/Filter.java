package com.marcinziolo.assignment.filter;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

class Filter implements IFilter {
    public Collection<Element> filterElements(@Nonnull Collection<Element> elements) {
        return elements.stream()
                .filter(element -> element.age > 20)
                .map(ElementWrapper::new)
                .distinct()
                .map(ElementWrapper::getElement)
                .collect(Collectors.toList());
    }

    private class ElementWrapper {
        private Element element;

        ElementWrapper(Element element) {
            this.element = element;
        }

        Element getElement() {
            return element;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ElementWrapper that = (ElementWrapper) o;
            return Objects.equals(element.num, that.element.num);
        }

        @Override
        public int hashCode() {
            return Objects.hash(element.num);
        }
    }
}
