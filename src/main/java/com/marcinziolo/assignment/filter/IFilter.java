package com.marcinziolo.assignment.filter;

import java.util.Collection;

public interface IFilter {
    Collection<Element> filterElements(Collection<Element> elements);
}
