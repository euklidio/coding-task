package com.marcinziolo.assignment.filter;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.testng.Assert.assertEquals;

public class FilterTest {

    private IFilter filter;

    @BeforeMethod
    public void setUp() {
        filter = new Filter();
    }

    @Test(dataProvider = "collections")
    public void shouldFilterCollection(Collection<Element> inputCollection, Collection<Element> uniqueCollection) {
        assertEquals(filter.filterElements(inputCollection), uniqueCollection);
    }

    @DataProvider(name = "collections")
    public static Object[][] collections(){
        return new Object[][] {
                {new ArrayList<>(), new ArrayList<>()},
                {FilterTestData.ELEMENTS, FilterTestData.UNIQUE_ELEMENTS},
        };

    }
}