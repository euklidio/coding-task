package com.marcinziolo.assignment.filter;

import java.util.Arrays;
import java.util.List;

public class FilterTestData {
    private static final Element ELEMENT_1 = new Element("A", 1, 23);
    private static final Element ELEMENT_2 = new Element("B", 2, 24);
    private static final Element ELEMENT_3 = new Element("C", 2, 19);
    private static final Element ELEMENT_4 = new Element("D", 2, 26);
    private static final Element ELEMENT_5 = new Element("E", 5, 27);

    static final List<Element> ELEMENTS = Arrays.asList(ELEMENT_1, ELEMENT_2, ELEMENT_3, ELEMENT_4, ELEMENT_5);
    static final List<Element> UNIQUE_ELEMENTS = Arrays.asList(ELEMENT_1, ELEMENT_2, ELEMENT_5);
}
