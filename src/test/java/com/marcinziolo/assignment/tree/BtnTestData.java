package com.marcinziolo.assignment.tree;

class BtnTestData {
    static final Btn SINGLE_BTN = new Btn(6, null, null);

    static final Btn BTN_V1 = new Btn(6, new Btn(11, null, null), new Btn(6, null, null));
    static final Btn BTN_V2 = new Btn(6, new Btn(11, null, null), new Btn(7, null, null));
    static final Btn BTN_V3 = new Btn(6, new Btn(11, new Btn(8, null, null), null), new Btn(7, null, new Btn(9, null, null)));
    static final Btn BTN_V3_COPY = new Btn(6, new Btn(11, new Btn(8, null, null), null), new Btn(7, null, new Btn(9, null, null)));
    static final Btn CYCLIC_BTN_V1 = new Btn(7, null, null);
    static {
        CYCLIC_BTN_V1.setLeft(CYCLIC_BTN_V1);
        CYCLIC_BTN_V1.setRight(CYCLIC_BTN_V1);
    }

    static final Btn CYCLIC_BTN_V2 = new Btn(-2, null, new Btn( 5, null, null));
    static {
        CYCLIC_BTN_V2.getRight().setLeft(CYCLIC_BTN_V2);
    }

    static final Btn CYCLIC_BTN_V3 = new Btn(-2, new Btn( -2, null, new Btn(-2, null, null)), null);
    static {
        CYCLIC_BTN_V3.getLeft().setLeft(CYCLIC_BTN_V3);
    }

    static final Btn CYCLIC_BTN_V4 = new Btn(-2, new Btn( -2, new Btn(-2, null, null), null), null);
    static {
        CYCLIC_BTN_V4.getLeft().setRight(CYCLIC_BTN_V4.getLeft());
    }

    static final Btn CYCLIC_BTN_V4_COPY = new Btn(-2, new Btn( -2, new Btn(-2, null, null), null), null);
    static {
        CYCLIC_BTN_V4_COPY.getLeft().setRight(CYCLIC_BTN_V4_COPY.getLeft());
    }

    // left and right go to the same node
    static final Btn STRANGE_BTN = new Btn(-2, new Btn(6, null, null), null);
    static {
        STRANGE_BTN.setRight(STRANGE_BTN.getLeft());
    }
}
