package com.marcinziolo.assignment.tree;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class BtnComparatorTest {

    IBtnComparator btnComparator;

    @BeforeMethod
    public void setUp() {
        btnComparator = new BtnComparator();
    }

    @Test(dataProvider = "nodes")
    public void shouldCompareTrees(Btn btnFirst, Btn btnSecond, boolean expectedResult) {
        assertEquals(btnComparator.compare(btnFirst, btnSecond), expectedResult);
    }

    @DataProvider(name = "nodes")
    public static Object[][] btns(){

        return new Object[][] {
                {null, BtnTestData.STRANGE_BTN, false},
                {BtnTestData.CYCLIC_BTN_V1, null, false},
                {BtnTestData.SINGLE_BTN, BtnTestData.BTN_V1, false},
                {BtnTestData.BTN_V2, BtnTestData.BTN_V1, false},
                {BtnTestData.BTN_V3, BtnTestData.BTN_V1, false},
                {BtnTestData.BTN_V3, BtnTestData.BTN_V2, false},
                {BtnTestData.SINGLE_BTN, BtnTestData.BTN_V1, false},
                {BtnTestData.SINGLE_BTN, BtnTestData.CYCLIC_BTN_V3, false},
                {BtnTestData.CYCLIC_BTN_V2, BtnTestData.CYCLIC_BTN_V3, false},
                {BtnTestData.CYCLIC_BTN_V1, BtnTestData.CYCLIC_BTN_V3, false},
                {BtnTestData.CYCLIC_BTN_V2, BtnTestData.CYCLIC_BTN_V1, false},
                {BtnTestData.CYCLIC_BTN_V3, BtnTestData.CYCLIC_BTN_V4, false},
                {BtnTestData.CYCLIC_BTN_V4, BtnTestData.CYCLIC_BTN_V3, false},
                {BtnTestData.BTN_V3, BtnTestData.CYCLIC_BTN_V1, false},

                {null, null, true},
                {BtnTestData.SINGLE_BTN, BtnTestData.SINGLE_BTN, true},
                {BtnTestData.BTN_V1, BtnTestData.BTN_V1, true},
                {BtnTestData.BTN_V2, BtnTestData.BTN_V2, true},
                {BtnTestData.BTN_V3, BtnTestData.BTN_V3, true},
                {BtnTestData.BTN_V3, BtnTestData.BTN_V3_COPY, true},
                {BtnTestData.CYCLIC_BTN_V1, BtnTestData.CYCLIC_BTN_V1, true},
                {BtnTestData.CYCLIC_BTN_V2, BtnTestData.CYCLIC_BTN_V2, true},
                {BtnTestData.CYCLIC_BTN_V3, BtnTestData.CYCLIC_BTN_V3, true},
                {BtnTestData.CYCLIC_BTN_V4, BtnTestData.CYCLIC_BTN_V4_COPY, true},
                {BtnTestData.STRANGE_BTN, BtnTestData.STRANGE_BTN, true},
        };

    }
}